#include <stdlib.h>
#include <stdio.h>
int main(){	
	int words = 0;
	int lines = 0;	
	char line[256];
	char delim[7] = " .,!?;\n";
	char *ptr;	
	
	FILE *file;
	char filename[100];

	printf("Enter file name\n");
	scanf ("%s", filename);
	
        file = fopen(filename, "r");
        if (file == NULL) {
	        fprintf(stderr, "File %s could not be opened\n", filename);
       		 exit(1);
	}

	while(fgets(line, 300,  (file))){
		ptr = line;
		while(strtok(ptr, delim)!= NULL){
			words++;
			ptr = NULL;
		}
		lines++;
	}
	printf("%d words \n", words);
	printf("%d lines \n", lines);
	fclose(file);

}
